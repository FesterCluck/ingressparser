/**
Parses Ingress JSON data and populates the model
**/

//Model objects
var players = new Object();
/*
id
team
name
level
plexts[]
lastSeen
*/
var portals = new Object();
/*   *=VOLATILE
id
team
name
lat
lng
level
*links
*fields{}
*fieldsNum
*ap
resoCount
resonators[]
mods[]
plexts[]
captured
lastSeen
*/
var plexts = new Object();
/*
id
playerid
type
action
portalids[]
epoch
*/
var chats = new Object();
/*
id
team
text
epoch
*/

//Ingress IndexedDB for persistent data
var ingressdb = {};
ingressdb.indexedDB = {};

window.indexedDB = window.indexedDB || window.webkitIndexedDB ||
                   window.mozIndexedDB;
                   
if ('webkitIndexedDB' in window) {
  window.IDBTransaction = window.webkitIDBTransaction;
  window.IDBKeyRange = window.webkitIDBKeyRange;
}

ingressdb.indexedDB.db = null;

ingressdb.indexedDB.open = function() {
    var version = 13;
    var request = indexedDB.open("ingress", version);
  
    request.onupgradeneeded = function(e) {
        console.log("onupgradeneeded");
        var db = e.target.result;
        e.target.transaction.onerror = ingressdb.indexedDB.onerror;

        if(db.objectStoreNames.contains("players"))
          db.deleteObjectStore("players");
        if(db.objectStoreNames.contains("plexts"))
          db.deleteObjectStore("plexts");
        if(db.objectStoreNames.contains("portals"))
          db.deleteObjectStore("portals");
        if(db.objectStoreNames.contains("resonators"))
          db.deleteObjectStore("resonators");
          
        var playerstore = db.createObjectStore("players", {keyPath: "id"});
        playerstore.createIndex('playerTime', ['lastSeen']);
          
        var plextstore = db.createObjectStore("plexts", {keyPath: "id"});
        plextstore.createIndex('plextTime', ['epoch']);
          
        var portalstore = db.createObjectStore("portals", {keyPath: "id"});
        portalstore.createIndex('portalTime', ['lastSeen']);
    }
  
    request.onsuccess = function(e) {
        //When DB is successfully opened, load persistent data into model
        ingressdb.indexedDB.db = e.target.result;
        ingressdb.indexedDB.getAllPlayers();
        ingressdb.indexedDB.getAllPortals();
        ingressdb.indexedDB.getAllPlexts();
    }

  request.onerror = ingressdb.indexedDB.onerror;
}

ingressdb.indexedDB.addPlayer = function(player) {
//console.log("Adding Player");
  var db = ingressdb.indexedDB.db;
  var trans = db.transaction(["players"], "readwrite");
  var store = trans.objectStore("players");
  var request = store.put({
    "id": player.id,
    "name" : player.name,
    "names" : player.names,
    "level" : player.level,
    "team" : player.team,
    "lastSeen" : player.lastSeen,
    "plexts" : player.plexts
  });
    
  request.onsuccess = function(e) {
    //ingressdb.indexedDB.getAllPlayers();
    //console.log("PlayerAdded");
  }

  request.onerror = function(e) {
    console.log(e.value);
  }
}

ingressdb.indexedDB.getAllPlayers = function() {
  var db = ingressdb.indexedDB.db;
  var trans = db.transaction(["players"], "readwrite");
  var store = trans.objectStore("players");

  // Get everything in the store;
  var keyRange = IDBKeyRange.lowerBound(0);
  var cursorRequest = store.openCursor(keyRange);

  cursorRequest.onsuccess = function(e) {
    var cursor = e.target.result;
    if(cursor) {
        players[cursor.value.id] = cursor.value;
		//Conversion for old data
		if(typeof cursor.value.names == 'undefined') {
			players[cursor.value.id].names = [];
			if(players[cursor.value.id].name != cursor.value.id) {
				players[cursor.value.id].names[players[cursor.value.id].names.length] = players[cursor.value.id].name;
			}
		}
        cursor.continue();
    } else {
        /*injector.invoke(['MyService', function(MyService){
            MyService.send();
        }]);*/
    }
  };

  cursorRequest.onerror = ingressdb.indexedDB.onerror;
};

ingressdb.indexedDB.getAllPortals = function() {
  var db = ingressdb.indexedDB.db;
  var trans = db.transaction(["portals"], "readwrite");
  var store = trans.objectStore("portals");

  // Get everything in the store;
  var keyRange = IDBKeyRange.lowerBound(0);
  var cursorRequest = store.openCursor(keyRange);

  cursorRequest.onsuccess = function(e) {
    var cursor = e.target.result;
    if(cursor) {
        portals[cursor.value.id] = cursor.value;
        portals[cursor.value.id].ap = 0;
        portals[cursor.value.id].links = 0;
        portals[cursor.value.id].fields = new Object();
        portals[cursor.value.id].fieldsNum = 0;
		updatePortalAP(cursor.value.id);
		//Conversion for old data
		if(typeof portals[cursor.value.id].plexts == 'undefined') {
			portals[cursor.value.id].plexts = [];
		}
        cursor.continue();
    } else {
        /*injector.invoke(['MyService', function(MyService){
            MyService.send();
        }]);*/
    }
  };

  cursorRequest.onerror = ingressdb.indexedDB.onerror;
};

ingressdb.indexedDB.getAllPlexts = function() {
  var db = ingressdb.indexedDB.db;
  var trans = db.transaction(["plexts"], "readwrite");
  var store = trans.objectStore("plexts");

  // Get everything in the store;
  var keyRange = IDBKeyRange.lowerBound(0);
  var cursorRequest = store.openCursor(keyRange);

  cursorRequest.onsuccess = function(e) {
    var cursor = e.target.result;
    if(cursor) {
        plexts[cursor.value.id] = cursor.value;
        cursor.continue();
    } else {
        /*injector.invoke(['MyService', function(MyService){
            MyService.send();
        }]);*/
    }
  };

  cursorRequest.onerror = ingressdb.indexedDB.onerror;
};

ingressdb.indexedDB.clean = function() {
	var plextClean = [];
	
	var twoWeeks = Date.now() - 1209600000;
	var oneWeeks = Date.now() - 604800000;
	
	for(var id in plexts) {
		var plext = plexts[id];
		if(plext.epoch < oneWeeks) {
			plextClean[plextClean.length] = plext.id;
		}
	}


  var db = ingressdb.indexedDB.db;
  var trans = db.transaction(["plexts"], "readwrite");
  var store = trans.objectStore("plexts");

	for(var i = 0; i < plextClean.length; i++) {
		var id = plextClean[i];
		delete plexts[id];
		var req = store.delete(id);
		req.onsuccess = function (ev) {
            console.log("plext with key = " + id + "deleted.");
        }
        req.onerror = function (ev) {
            console.log("Failed to delete record." + "  Error: " + ev.message);
        }
	}


};

ingressdb.indexedDB.addPortal = function(portal) {
  var db = ingressdb.indexedDB.db;
  var trans = db.transaction(["portals"], "readwrite");
  var store = trans.objectStore("portals");
  var request = store.put({
    "id": portal.id,
    "name" : portal.name,
    "lat" : portal.lat,
    "lng" : portal.lng,
    "team" : portal.team,
    "level" : portal.level,
    "resoCount" : portal.resoCount,
    "resonators" : portal.resonators,
    "mods" : portal.mods,
    "plexts" : portal.plexts,
    "lastSeen" : portal.lastSeen,
    "captured" : portal.captured
  });

  request.onsuccess = function(e) {
  }

  request.onerror = function(e) {
    console.log(e.value);
  }
}

ingressdb.indexedDB.addPlext = function(plext) {
  var db = ingressdb.indexedDB.db;
  var trans = db.transaction(["plexts"], "readwrite");
  var store = trans.objectStore("plexts");
  var request = store.put({
    "id": plext.id,
    "playerid" : plext.playerid,
    "type" : plext.type,
    "action" : plext.action,
    "portalids" : plext.portalids,
    "epoch" : plext.epoch
  });

  request.onsuccess = function(e) {
  }

  request.onerror = function(e) {
    console.log(e.value);
  }
}

ingressdb.indexedDB.open();

//Add a function to object to calculate the size of a hash
Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

//Main parsing function
function parse(object)
{
    //Exit out if null
    if(object == null)
        return;
        
    //Not interested in game basket stuff yet
	if(typeof object.gameBasket  != 'undefined') {
		if(typeof object.gameBasket.deletedEntityGuids != 'undefined')
			if(object.gameBasket.deletedEntityGuids.length > 0)
				console.log("------deletedEntityGuids PARSING");

		if(typeof object.gameBasket.gameEntities != 'undefined')
			if(object.gameBasket.gameEntities.length > 0)
				console.log("------gameEntities PARSING");
			
		if(typeof object.gameBasket.inventory != 'undefined')
			if(object.gameBasket.inventory.length > 0)
				console.log("------inventory PARSING");
	}

	if( Object.prototype.toString.call( object.result ) === '[object Array]' ) {
		for (var i = 0; i < object.result.length; i++) {
			if(typeof object.result[i].guid != 'undefined') {
				//playerGUIDs response
                updatePlayerName(object.result[i].guid, object.result[i].nickname);
			} else {
                //Plext response
				parsePlext(object.result[i]);
			}
		}
	} else if(typeof object.result != 'undefined') {
        //Thinned entities response
		parseMap(object.result);
	}
}

function parseMap(result) {
	for(var key in result.map) {
		var entities = result.map[key];
		for(var num in entities.gameEntities) {
			var entity = entities.gameEntities[num];
			if(typeof entity[2].resonatorArray != 'undefined') {
				//PORTAL
				updatePortalName(entity[0],entity[2].portalV2.descriptiveText.TITLE);
				updatePortalTeam(entity[0],entity[2].controllingTeam.team);
				updatePortalLatLong(entity[0],entity[2].locationE6.latE6,entity[2].locationE6.lngE6);
				updatePortalLinks(entity[0],entity[2].portalV2.linkedEdges.length);
                
				if(typeof entity[2].captured != 'undefined') {
					var days = Math.round((Date.now() - entity[2].captured.capturedTime) / 1000 / 60 / 60 / 24*100)/100;
					days.toFixed(2);
					
					updatePortalCaptured(entity[0], days);
                } else {
					updatePortalCaptured(entity[0], 0);
				}
				
				parseResonators(entity[0], entity[2].controllingTeam.team, entity[2].resonatorArray.resonators);
				parseMods(entity[0], entity[2].portalV2.linkedModArray);
	
                updatePortalAP(entity[0]);
                updatePortalSeen(entity[0]);
            }
            if(typeof entity[2].capturedRegion != 'undefined') {
                //FIELD
                updatePortalFields(entity[2].capturedRegion.vertexA.guid, entity[0]);
                updatePortalFields(entity[2].capturedRegion.vertexB.guid, entity[0]);
                updatePortalFields(entity[2].capturedRegion.vertexC.guid, entity[0]);
            }
		}
	}
}

function parseMods(portalId, mods) {
/*
									{
										"installingUser" : "e91d29a10edb41d68cde077b5c2c765b.c",
										"displayName" : "Portal Shield",
										"type" : "RES_SHIELD",
										"stats" : {
											"REMOVAL_STICKINESS" : "0",
											"MITIGATION" : "20"
										},
										"rarity" : "RARE"
									},
									{
										"installingUser" : "c1ca8489ce1647f38c5192ae2499961e.c",
										"type" : "HEATSINK",
										"stats" : {
											"REMOVAL_STICKINESS" : "0",
											"HACK_SPEED" : "200000"
										},
										"displayName" : "Heat Sink",
										"rarity" : "COMMON"
									}
									*/
	for(var id in mods) {
		if(mods[id] == null) {
			deleteMod(portalId, id);
			continue;
		}
		
		var mod = mods[id];
		
		var type = "X";
		switch(mod.type) {
                case "RES_SHIELD": 	type = "S";break;
                case "FORCE_AMP": 	type = "A";break;
                case "TURRET": 		type = "T";break;
                case "HEATSINK": 	type = "H";break;
                case "MULTIHACK": 	type = "M";break;
                case "LINK_AMPLIFIER": 	type = "L";break;
                default: console.log("Unknown Mod: " + mod.type);
            }
			
		var rarity = "X";
		switch(mod.rarity) {
                case "COMMON": 	rarity = "C";break;
                case "RARE": 	rarity = "R";break;
                case "VERY_RARE": 	rarity = "V";break;
                default: console.log("Unknown Rarity: [" + mod.rarity + "]");
            }
		
		updateMod(portalId, id, type, mod.installingUser, rarity);
	}
}


function updateMod(portalid, slot, type, playerid, rarity) {
    ensureMod(portalid,slot);
    
    //If already updated exit early
    if(playerid == portals[portalid].mods[slot].playerid &&
	   type == portals[portalid].mods[slot].type &&
	   rarity == portals[portalid].mods[slot].rarity)
        return;
    
    portals[portalid].mods[slot].playerid = playerid;
    portals[portalid].mods[slot].type = type;
    portals[portalid].mods[slot].rarity = rarity;
    
    portals[portalid].lastSeen = Date.now();
    ingressdb.indexedDB.addPortal(portals[portalid]);
}

function deleteMod(portalid, slot) {
    ensureMod(portalid,slot);
    
    //If already deleted, exit early
    if(portals[portalid].mods[slot].type == "_")
        return;
     
    portals[portalid].mods[slot].playerid = "";
    portals[portalid].mods[slot].type = "";
    
    portals[portalid].lastSeen = Date.now();
    
    //Update portal in indexedDB
    ingressdb.indexedDB.addPortal(portals[portalid]);
}

function parseResonators(portalId, team, resos) {
    var resarray = new Array();
    for(var i = 0; i < 8; i++)
        resarray[i] = null;
	for(var id in resos) {
        if(resos[id] == null)
            continue;
        resarray[resos[id].slot] = resos[id];
    }
    for(var j = 0; j < 8; j++) {
        var reso = resarray[j];
		if(reso == null) {
			deleteResonator(portalId,j);
			continue;
		}
        
		var insert = { "portal" : portalId, "slot": reso.slot, "id" : reso.id, "player": reso.ownerGuid, "distance": reso.distanceToPortal, "level" : reso.level, "energy": reso.energyTotal};
		updateResonator(portalId, reso.slot, reso.ownerGuid, reso.distanceToPortal, reso.level, reso.energyTotal);
		
        //We can infer player team based on resonator ownership
		
		updatePlayerTeam(reso.ownerGuid,team);
        
		//Used for resolving player names
		ensurePlayer(reso.ownerGuid);
		players[reso.ownerGuid].portalid = portalId;
	}
}

function deleteResonator(portalid, slot) {
    ensureResonator(portalid,slot);
    
    //If already deleted, exit early
    if(portals[portalid].resonators[slot].level == -1)
        return;
        
    portals[portalid].resonators[slot].playerid = "";
    portals[portalid].resonators[slot].level = -1;
    portals[portalid].resonators[slot].distance = -1;
    portals[portalid].resonators[slot].energy = -1;
    
    var count = 0;
    var level = 0;
    
    //Recalculate portal level and resonator count
    for(var i = 0; i < 8; i++) {
        ensureResonator(portalid,i);
        if(portals[portalid].resonators[i].level > 0) {
            count = count + 1;
            level = level + portals[portalid].resonators[i].level;
        }
    }
    
    portals[portalid].resoCount = count;
    portals[portalid].level = Math.floor(level / 8);
    portals[portalid].lastSeen = Date.now();
    
    //Update portal in indexedDB
    ingressdb.indexedDB.addPortal(portals[portalid]);
}

function updateResonator(portalid, slot, playerid, distance, level, energy) {
    ensureResonator(portalid,slot);
    
    //If already updated exit early
    if(playerid == portals[portalid].resonators[slot].playerid && level == portals[portalid].resonators[slot].level && energy == portals[portalid].resonators[slot].energy)
        return;
    
    portals[portalid].resonators[slot].playerid = playerid;
    portals[portalid].resonators[slot].level = level;
    portals[portalid].resonators[slot].distance = distance;
    portals[portalid].resonators[slot].energy = energy;
    
    var count = 0;
    var level = 0;
    
    //Recalculate portal level and resonator count
    for(var i = 0; i < 8; i++) {
        ensureResonator(portalid,i);
        if(portals[portalid].resonators[i].level > 0) {
            count = count + 1;
            level = level + portals[portalid].resonators[i].level;
        }
    }
    
    portals[portalid].resoCount = count;
    portals[portalid].level = Math.floor(level / 8);
    portals[portalid].lastSeen = Date.now();
    ingressdb.indexedDB.addPortal(portals[portalid]);
}

//Constants for AP value
var PLACING_RESONATORS = 1750;
var DESTROY_RESONATOR = 75;
var DESTROY_LINK = 187;
var DESTROY_FIELD = 750;

function updatePortalFields(portalid, fieldid) {
    ensurePortal(portalid);
    //Create a new field with the ID so we don't count the same field twice.
    portals[portalid].fields[fieldid] = 1;
    //Update portal field counts
    portals[portalid].fieldsNum = Object.size(portals[portalid].fields);
    updatePortalAP(portalid);
}

function updatePortalLinks(portalid, links) {
    ensurePortal(portalid);
    portals[portalid].links = links;
}

function updatePortalAP(portalid) {
    ensurePortal(portalid);
    //Update AP values for the portal
    var ap = PLACING_RESONATORS;
    ap = ap + portals[portalid].resoCount * DESTROY_RESONATOR;
    ap = ap + portals[portalid].links * DESTROY_LINK;
    ap = ap + portals[portalid].fieldsNum * DESTROY_FIELD;
    portals[portalid].ap = ap;
}
	
	
function updatePortalSeen(portalid) {
    ensurePortal(portalid);
    //Update AP values for the portal
    portals[portalid].lastSeen = Date.now();
    ingressdb.indexedDB.addPortal(portals[portalid]);
}

function ensureMod(portalid, slot) {
    ensurePortal(portalid);
	if(typeof portals[portalid].mods == 'undefined')
		portals[portalid].mods = new Array();
	
    //If the mod object exists, exit.
    if(typeof portals[portalid].mods[slot] != 'undefined')
        return;
    //Otherwise create a new mod object
    portals[portalid].mods[slot] = new Object();
    portals[portalid].mods[slot].slot = slot;
    portals[portalid].mods[slot].type = "";
    portals[portalid].mods[slot].playerid = "";
}

function ensureResonator(portalid, slot) {
    ensurePortal(portalid);
    //If the resonator object exists, exit.
    if(typeof portals[portalid].resonators[slot] != 'undefined')
        return;
    //Otherwise create a new resonator object
    portals[portalid].resonators[slot] = new Object();
    portals[portalid].resonators[slot].slot = slot;
    portals[portalid].resonators[slot].portalid = "";
    portals[portalid].resonators[slot].playerid = "";
    portals[portalid].resonators[slot].level = -1;
    portals[portalid].resonators[slot].distance = -1;
    portals[portalid].resonators[slot].energy = -1;
}

function updatePlayerLevel(guid, level) {
	ensurePlayer(guid);
    //If player level is already equal or higher than the reported level, just exit
    if(level > players[guid].level) {
        console.log("Player[" + players[guid].id + "].level=" + level);
        players[guid].level = level;
        //Update player in indexedDB
        players[guid].lastSeen = Date.now();
        ingressdb.indexedDB.addPlayer(players[guid]);
    }
}

function updatePlayerTeam(guid, team) {
	ensurePlayer(guid);
    //If player is already on this team, exit
    if(players[guid].team == team)
        return;
    console.log("Player[" + players[guid].id + "].team=" + team);
    players[guid].team = team;
    //Update player in indexedDB
    players[guid].lastSeen = Date.now();
    ingressdb.indexedDB.addPlayer(players[guid]);
}
	
function updatePlayerName(guid, name) {
	ensurePlayer(guid);
    if(players[guid].name == name)
        return
    console.log("Player[" + players[guid].id + "].name=" + name);
    players[guid].name = name;
	//Create list if doesn't exist
	if(typeof players[guid].names == 'undefined')
		players[guid].names = [];
	//log list and check names
	var has = false;
	for(var i = 0; i < players[guid].names.length; i++) {
		if(name == players[guid].names[i])
			has = true;
		console.log("Player[" + players[guid].id + "].names=" + players[guid].names[i]);
	}
	//add name if doesn't exist in list already
	if(!has)
		players[guid].names[players[guid].names.length] = name;
    //Update player in indexedDB
    players[guid].lastSeen = Date.now();
    ingressdb.indexedDB.addPlayer(players[guid]);
}

function updatePortalName(guid, name) {
    ensurePortal(guid);
    if(portals[guid].name == name)
        return
    console.log("Portal[" + portals[guid].id + "].name=" + name);
    portals[guid].name = name;
    portals[guid].lastSeen = Date.now();
    //Update portal in indexedDB
    ingressdb.indexedDB.addPortal(portals[guid]);
}

function updatePortalTeam(guid, team) {
    ensurePortal(guid);
    if(portals[guid].team == team)
        return
    console.log("Portal[" + portals[guid].id + "].team=" + team);
    portals[guid].team = team;
    portals[guid].lastSeen = Date.now();
    //Update portal in indexedDB
    ingressdb.indexedDB.addPortal(portals[guid]);
}

function updatePortalLatLong(guid, lat, lng) {
    ensurePortal(guid);
    if(portals[guid].lat == lat && portals[guid].lng == lng)
        return;
    console.log("Portal[" + portals[guid].id + "].lat=" + lat);
    console.log("Portal[" + portals[guid].id + "].lng=" + lng);
    portals[guid].lat = lat;
    portals[guid].lng = lng;
    portals[guid].lastSeen = Date.now();
    //Update portal in indexedDB
    ingressdb.indexedDB.addPortal(portals[guid]);
}

function updatePortalCaptured(guid, captured) {
    ensurePortal(guid);
    if(portals[guid].captured == captured)
        return;
    console.log("Portal[" + portals[guid].id + "].captured=" + captured);
    portals[guid].captured = captured;
    portals[guid].lastSeen = Date.now();
    //Update portal in indexedDB
    ingressdb.indexedDB.addPortal(portals[guid]);
}

function ensurePlayer(id) {
    //If player already exists, exit
    if(typeof players[id] != 'undefined')
        return;
    //Otherwise create a new player
    players[id] = new Object();
    players[id].id = id;
    players[id].team = "NEUTRAL";
    players[id].name = id;
    players[id].names = [];
    players[id].level = 0;
    players[id].plexts = [];
    players[id].lastSeen = Date.now();
}

function ensurePortal(id) {
    //If portal already exists, return
    if(typeof portals[id] != 'undefined')
        return;
    //Otherwise create a new portal with default values
    portals[id] = new Object();
    portals[id].id = id;
    portals[id].team = "NEUTRAL";
    portals[id].name = id;
    portals[id].lat = 0;
    portals[id].lng = 0;
    portals[id].level = 0;
    portals[id].links = 0;
    portals[id].fields = new Object();
    portals[id].fieldsNum = 0;
    portals[id].ap = 0;
    portals[id].resoCount = 0;
    portals[id].resonators = new Array();
    portals[id].mods = new Array();
    portals[id].captured = Date.now();
    portals[id].lastSeen = Date.now();
    portals[id].plexts = [];
}

//Function is unfinished
function updatePlext(id, plext) {
    //If comm message already exists, exit
    if(typeof plexts[id] != 'undefined')
        return;
    //Otherwise create a new comm object with default values
    plexts[id] = new Object();
    plexts[id].id = id;
    plexts[id].playerid = plext.playerid;
    plexts[id].type = plext.type;
    plexts[id].action = plext.action;
    plexts[id].portalids = plext.portalids;
    plexts[id].epoch = plext.epoch;
    ingressdb.indexedDB.addPlext(plexts[id]);
    
//Add plext to player	
    ensurePlayer(plexts[id].playerid);
    
    var player = players[plexts[id].playerid];
    player.plexts[player.plexts.length] = id;
    ingressdb.indexedDB.addPlayer(player);
	
	//console.log("Adding to portals: " + plexts[id].portalids);
//Add plext to portal
	for(var i = 0; i < plexts[id].portalids.length; i++) {
		var pid = plexts[id].portalids[i];
        
		ensurePortal(pid);
    
		var portal = portals[pid];
		portal.plexts[portal.plexts.length] = id;
		//console.log("portal[" + portal.id + "].plexts=" + id);
		ingressdb.indexedDB.addPortal(portal);
	}
}

function updateChat(id, plext) {
    //If comm message already exists, exit
    if(typeof chats[id] != 'undefined')
        return;
    //Otherwise create a new comm object with default values
    chats[id] = new Object();
    chats[id].id = id;
    chats[id].team = plext.team;
    chats[id].text = plext.text;
    chats[id].epoch = plext.epoch;
}

//Parse plext entries
function parsePlext(plext) {
	var plexid = plext[0];
	var timestamp = plext[1];
	var plexttext = JSON.stringify(plext);
	var guid = "";
	var playername = "";
	var action = "";
	var level = "";
	var type = "";
	var latitudes = [];
	var longitudes = [];
	var portalids = [];
	
	var text = plext[2].plext.text;
	var plexttype = plext[2].plext.plextType;
		
	if(plexttype == 'SYSTEM_BROADCAST') {
		if(text.indexOf("deployed") != -1) {
			action = "DEPLOYED";
		}
		if(text.indexOf("destroyed") != -1) {
			action = "DESTROYED";
		}
		if(text.indexOf("captured") != -1) {
			action = "CAPTURED";
			type = "PORTAL";
		}
		if(text.indexOf("Link") != -1) {
			type = "LINK";
		}
		if(text.indexOf("created") != -1) {
			action = "CREATED";
		}
		if(text.indexOf("linked") != -1) {
			action = "CREATED";
			type = "LINK";
		}
		if(text.indexOf("Resonator") != -1) {
			type = "RESONATOR";
		}
		if(text.indexOf("Control Field") != -1) {
			type = "CONTROL_FIELD";
		}
	}

	for (var j = 0; j < plext[2].plext.markup.length; j++) {
		var markup = plext[2].plext.markup[j];
		
		if(markup[0] == 'PLAYER') {
			var player = markup[1];
			
			updatePlayerTeam(player.guid, player.team);
			updatePlayerName(player.guid, player.plain);
			
			guid = player.guid;
			playername = player.plain;
			
		} else if(markup[0] == 'SENDER') {
			var sender = markup[1];
			
			updatePlayerTeam(sender.guid, sender.team);
			updatePlayerName(sender.guid, sender.plain.split(":")[0]);
		} else if(markup[0] == 'PORTAL') {
			var portal = markup[1];
			
			updatePortalName(portal.guid,portal.name);
			//updatePortalLatLong(portal.guid,portal.lngE6);
			
			portalids[portalids.length] = portal.guid;
			latitudes[latitudes.length] = portal.latE6;
			longitudes[longitudes.length] = portal.lngE6;
		} else if(markup[0] == 'TEXT') {
			var text = markup[1].plain;
			if(text.indexOf("L") != -1 && text.length == 2) {
				level = text.substring(1);
			}
		}
	}
	
    //Only update from system broadcasts, don't let player messages through
	if(plexttype == 'SYSTEM_BROADCAST') {
		var insert = { "id":plexid, "playerid":guid, "type":type, "action":action, "level":level, "portalids":portalids, "epoch":timestamp};
		updatePlext(plexid, insert);
        //console.log(level + action + type);
		if(action == 'DEPLOYED' && type == 'RESONATOR' && level != ""){
            //We can get player level from plexts as well if they deployed resonators
            //previously and we scroll the comm up
			updatePlayerLevel(guid,level);
        }
	}
	if(plexttype == 'PLAYER_GENERATED') {
		var insert = { "id":plexid, "team":plext[2].plext.team, "text":plext[2].plext.text, "epoch":timestamp};
		updateChat(plexid, insert);
	}
}