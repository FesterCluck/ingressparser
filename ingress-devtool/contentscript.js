console.log("Loading");

var timer = 0;
chrome.extension.onMessage.addListener(
  function(request, sender, sendResponse) {
    
	if(request.scroll == 1) {
		if (!timer) {
			timer = setInterval(function() {
				document.getElementById("plext_container").scrollTop = 150;
				document.getElementById("plext_container").scrollTop = 0;
			}, 10000);
		}
	} else {
		if (timer) {
            clearInterval(timer);
            timer = 0;
        }
	}
    sendResponse({farewell: "goodbye"});
  });