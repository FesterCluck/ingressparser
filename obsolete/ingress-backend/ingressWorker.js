/*
MIT LICENSE
Copyright (c) 2007 Monsur Hossain (http://www.monsur.com)

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
var CachePriority = {
    Low: 1,
    Normal: 2,
    High: 4
};
function Cache(maxSize) {
    this.items = {};
    this.count = 0;
    if (maxSize == null)
        maxSize = -1;
    this.maxSize = maxSize;
    this.fillFactor = .75;
    this.purgeSize = Math.round(this.maxSize * this.fillFactor);
    
    this.stats = {};
    this.stats.hits = 0;
    this.stats.misses = 0;
}
Cache.prototype.getItem = function(key) {
    var item = this.items[key];
    
    if (item != null) {
        if (!this._isExpired(item)) {
            item.lastAccessed = new Date().getTime();
        } else {
            this._removeItem(key);
            item = null;
        }
    }
    
    var returnVal = null;
    if (item != null) {
        returnVal = item.value;
        this.stats.hits++;
    } else {
        this.stats.misses++;
    }
    return returnVal;
};
Cache.prototype.setItem = function(key, value, options) {

    function CacheItem(k, v, o) {
        if ((k == null) || (k == ''))
            throw new Error("key cannot be null or empty");
        this.key = k;
        this.value = v;
        if (o == null)
            o = {};
        if (o.expirationAbsolute != null)
            o.expirationAbsolute = o.expirationAbsolute.getTime();
        if (o.priority == null)
            o.priority = CachePriority.Normal;
        this.options = o;
        this.lastAccessed = new Date().getTime();
    }
    if (this.items[key] != null)
        this._removeItem(key);
    this._addItem(new CacheItem(key, value, options));
    if ((this.maxSize > 0) && (this.count > this.maxSize)) {
        this._purge();
    }
};
Cache.prototype.clear = function() {

    // loop through each item in the cache and remove it
    for (var key in this.items) {
      this._removeItem(key);
    }  
};
Cache.prototype._purge = function() {
    var tmparray = new Array();
    for (var key in this.items) {
        var item = this.items[key];
        if (this._isExpired(item)) {
            this._removeItem(key);
        } else {
            tmparray.push(item);
        }
    }
    if (tmparray.length > this.purgeSize) {
        tmparray = tmparray.sort(function(a, b) { 
            if (a.options.priority != b.options.priority) {
                return b.options.priority - a.options.priority;
            } else {
                return b.lastAccessed - a.lastAccessed;
            }
        });
        while (tmparray.length > this.purgeSize) {
            var ritem = tmparray.pop();
            this._removeItem(ritem.key);
        }
    }
};
Cache.prototype._addItem = function(item) {
    this.items[item.key] = item;
    this.count++;
};
Cache.prototype._removeItem = function(key) {
    var item = this.items[key];
    delete this.items[key];
    this.count--;
    
    // if there is a callback function, call it at the end of execution
    if (item.options.callback != null) {
        var callback = function() {
            item.options.callback(item.key, item.value);
        };
        setTimeout(callback, 0);
    }
};
Cache.prototype._isExpired = function(item) {
    var now = new Date().getTime();
    var expired = false;
    if ((item.options.expirationAbsolute) && (item.options.expirationAbsolute < now)) {
        expired = true;
    } 
    if (!expired && (item.options.expirationSliding)) {
        var lastAccess = item.lastAccessed + (item.options.expirationSliding * 1000);
        if (lastAccess < now) {
            expired = true;
        }
    }
    return expired;
};
Cache.prototype.toHtmlString = function() {
    var returnStr = this.count + " item(s) in cache<br /><ul>";
    for (var key in this.items) {
        var item = this.items[key];
        returnStr = returnStr + "<li>" + item.key.toString() + " = " + item.value.toString() + "</li>";
    }
    returnStr = returnStr + "</ul>";
    return returnStr;
};

var options = {
  // time in ms when the event loop is considered blocked
  blockThreshold: 10
};

require('nodefly').profile(
    'eafb889dd36b2e6d8507969ba642fa7d',
    "ingressWorker",
    options // optional
);

var mysql      = require('mysql');

var pool = mysql.createPool({
		host     : 'localhost',
		user     : 'ingress',
		password : 'ingress12345',
		database : 'ingress_data'
	});

console.log("Startup");

var queries = [];

var processQueries = function() {
	if(queries.length == 0) {
		//console.log("no queries");
		setTimeout(processQueries, 2000);
        return;
	}
	
	var query = queries.shift();
	
	//console.log("Query Start " + queries.length + " left");
	
	var startTime = new Date();
	
	pool.getConnection(function(err, connection) {
		connection.query(query.sql, query.args, function(err) {
		
			connection.end();
			
			var endTime = new Date();
			
			//if((endTime - startTime) > 100)
			console.log('[' + queries.length + '] processed query in ' + (endTime - startTime) + 'ms ' + query.sql );
			console.log('Params: ' + JSON.stringify(query.args));

			if(err != null) {
				console.log('[' + queries.length + '] processed query in ' + (endTime - startTime) + 'ms ' + query.sql );
				console.log(JSON.stringify(query.args));
				console.log(JSON.stringify(err));
				//console.log('Processed query in ' + (endTime - startTime) + 'ms (' 
				//+ (1 / ((endTime - startTime) / 1000)) + ' qps) ' + query.sql + " " + JSON.stringify(query.args) );
				setTimeout(processQueries, 30000);
			} else
				setTimeout(processQueries, 0);
		
		});
	});
}

setTimeout(processQueries, 8000);

function queueQuery(sql, args) {
	var q = new Object();
	q.sql = sql;
	q.args = args;
	queries[queries.length] = q;
}

var Gearnode = require("gearnode");

worker = new Gearnode();
worker.addServer(); // use localhost

worker.addFunction("parse", "utf-8", function(payload, job){
	//console.log("GearnodeWorking");
	
    var obj = null;
	
    try {
        obj = JSON.parse(payload);
    } catch(err) {
       console.log("Parse error:" + payload);
    }
	
    if(obj != null)
        parse(obj);
		
	obj = null;
	
	//console.log("GearnodeDone");
	job.complete(payload);
});

/******************** PARSING *****************/

function parse(object)
{
    //Exit out if null
    if(object == null)
        return;

	if( object.result instanceof Array ) {
		for (var i = 0; i < object.result.length; i++) {
			if(typeof object.result[i].guid != 'undefined') {
				//playerGUIDs response
                updatePlayerName(object.result[i].guid, object.result[i].nickname);
			} else {
                //Plext response
				parsePlext(object.result[i]);
			}
		}
	} else if(typeof object.result != 'undefined') {
        //Thinned entities response
		parseMap(object.result);
	}
	
	deleteFlaggedLinks();
	deleteFlaggedFields();
}

function parseMap(result) {
	for(var key in result.map) {
		var entities = result.map[key];
		for(var num in entities.gameEntities) {
			var entity = entities.gameEntities[num];
			if(typeof entity[2].resonatorArray != 'undefined') {
				//PORTAL
				//console.log(entity[2].portalV2.descriptiveText.TITLE +
				//	" " + entity[0]);
				updatePortalName(entity[0],entity[2].portalV2.descriptiveText.TITLE);
				updatePortalTeam(entity[0],entity[2].controllingTeam.team);
				updatePortalLatLong(entity[0],entity[2].locationE6.latE6,entity[2].locationE6.lngE6);
                
				if(entity[2].controllingTeam.team != 'NEUTRAL')
					updatePortalCaptured(entity[0], entity[2].captured.capturedTime);
				
				parseResonators(entity[0], entity[2].controllingTeam.team, entity[2].resonatorArray.resonators);
				parseMods(entity[0], entity[2].portalV2.linkedModArray);
				flagLinksForRemoval(entity[0],entity[2].portalV2.linkedEdges);
				flagFieldsForRemoval(entity[0]);
    
                finalizePortal(entity[0]);
            }
            if(typeof entity[2].capturedRegion != 'undefined') {
                //FIELD
				updateField(entity[0], //ID
							entity[2].controllingTeam.team, //TEAM
							entity[2].capturedRegion.vertexA.guid, //PortalA
							entity[2].capturedRegion.vertexB.guid, //PortalB
							entity[2].capturedRegion.vertexC.guid, //PortalC
							entity[2].entityScore.entityScore, //MU's
							entity[2].creator.creatorGuid); //Creator
                //updatePortalFields(entity[2].capturedRegion.vertexA.guid, entity[0]);
                //updatePortalFields(entity[2].capturedRegion.vertexB.guid, entity[0]);
                //updatePortalFields(entity[2].capturedRegion.vertexC.guid, entity[0]);
				//entityScore
            }
            if(typeof entity[2].edge != 'undefined') {
                //LINK
				updateLink(	entity[0], //ID
							entity[2].controllingTeam.team, //TEAM
							entity[2].edge.originPortalGuid, //ORIGIN
							entity[2].edge.destinationPortalGuid, //REMOTE
							entity[2].creator.creatorGuid); //Creator
            }
		}
	}
}

function parseMods(portalId, mods) {
	for(var id in mods) {
		if(mods[id] == null) {
			deleteMod(portalId, id);
			continue;
		}

		updateMod(portalId, id, mods[id].type, mods[id].rarity, mods[id].installingUser);
	}
}

function parseResonators(portalId, team, resos) {
    var resarray = new Array();
    for(var i = 0; i < 8; i++)
        resarray[i] = null;
	for(var id in resos) {
        if(resos[id] == null)
            continue;
        resarray[resos[id].slot] = resos[id];
    }
    for(var j = 0; j < 8; j++) {
        var reso = resarray[j];
		if(reso == null) {
			deleteResonator(portalId,j);
			continue;
		}

        updateResonator(reso.id, portalId, reso.slot, reso.ownerGuid, reso.distanceToPortal, reso.level, reso.energyTotal);
		
        //We can infer player team based on resonator ownership
		updatePlayerTeam(reso.ownerGuid,team);
	}
}

//Parse plext entries
function parsePlext(plext) {
	var plexid = plext[0];
	var timestamp = plext[1];
	var action = "";
	var level = "";
	var type = "";
	var portalid1 = 0;
	var portalid2 = 0;
	//var plexttext = JSON.stringify(plext);
	var guid = "";
	var playername = "";
	
	var text = plext[2].plext.text;
	var plexttype = plext[2].plext.plextType;
		
	if(plexttype == 'SYSTEM_BROADCAST') {
		if(text.indexOf("deployed") != -1) {
			action = "DEPLOYED";
		}
		if(text.indexOf("destroyed") != -1) {
			action = "DESTROYED";
		}
		if(text.indexOf("captured") != -1) {
			action = "CAPTURED";
			type = "PORTAL";
		}
		if(text.indexOf("Link") != -1) {
			type = "LINK";
		}
		if(text.indexOf("created") != -1) {
			action = "CREATED";
		}
		if(text.indexOf("linked") != -1) {
			action = "CREATED";
			type = "LINK";
		}
		if(text.indexOf("Resonator") != -1) {
			type = "RESONATOR";
		}
		if(text.indexOf("Control Field") != -1) {
			type = "CONTROL_FIELD";
		}
	}

	for (var j = 0; j < plext[2].plext.markup.length; j++) {
		var markup = plext[2].plext.markup[j];
		
		if(markup[0] == 'PLAYER') {
			updatePlayerTeam(markup[1].guid, markup[1].team);
			updatePlayerName(markup[1].guid, markup[1].plain);
			
			guid = markup[1].guid;
			playername = markup[1].plain;
		} else if(markup[0] == 'SENDER') {
			updatePlayerTeam(markup[1].guid, markup[1].team);
			updatePlayerName(markup[1].guid, markup[1].plain.split(":")[0]);
		} else if(markup[0] == 'PORTAL') {
			//var portal = markup[1];
			
			updatePortalName(markup[1].guid, markup[1].name);
			//updatePortalLatLong(portal.guid,portal.lngE6);
			
			if(portalid1 == 0) {
				portalid1 = markup[1].guid;
			} else if(portalid2 == 0) {
				portalid2 = markup[1].guid;
			} else {
				console.log("ERROR Ran out of portal space.");
			}
		} else if(markup[0] == 'TEXT') {
			var text = markup[1].plain;
			if(text.indexOf("L") != -1 && text.length == 2) {
				level = text.substring(1);
			}
		}
	}
	
    //Only update from system broadcasts, don't let player messages through
	if(plexttype == 'SYSTEM_BROADCAST') {
		updatePlext(plexid, guid, type, action, portalid1, portalid2, timestamp);
        
		if(action == 'DEPLOYED' && type == 'RESONATOR' && level != ""){
            //We can get player level from plexts as well if they deployed resonators
            //previously and we scroll the comm up
			updatePlayerLevel(guid,level);
        }
	}
	if(plexttype == 'PLAYER_GENERATED') {
		//updateChat(plexid, insert);
	}
}

/******************** FIELD UPDATES *****************/

function updateField(id, team, portalA, portalB, portalC, MU, playerid) {
	queueQuery('/*ufield*/INSERT INTO fields (id, team, portalid1, portalid2, portalid3, mu, playerid, flagged) VALUES ' +
	'(?,?,?,?,?,?,?,0) ON DUPLICATE KEY UPDATE ' + 
	'team=VALUES(team), portalid1=VALUES(portalid1), portalid2=VALUES(portalid2), portalid3=VALUES(portalid3), mu=VALUES(mu), ' +
	'playerid=VALUES(playerid), flagged=VALUES(flagged)',
	[id,team,portalA,portalB,portalC,MU,playerid]);
}

function flagFieldsForRemoval(portalId) {
	queueQuery('/*flagAllFields*/UPDATE fields SET flagged = 1 WHERE (portalid1 = ? or portalid2 = ? or portalid3 = ?)',
	[portalId,portalId,portalId]);
}

function deleteFlaggedFields() {
	queueQuery('/*dfield*/DELETE FROM fields WHERE flagged=1',
	[]);
}

/******************** LINK UPDATES *****************/

function updateLink(id, team, origin, remote, playerid) {
	queueQuery('/*ulink*/INSERT INTO links (id, team, originid, otherid, playerid, flagged) VALUES ' +
	'(?,?,?,?,?,0) ON DUPLICATE KEY UPDATE team=VALUES(team), originid=VALUES(originid), otherid=VALUES(otherid), playerid=VALUES(playerid), flagged=VALUES(flagged)',
	[id,team,origin,remote,playerid]);
}

function flagLinksForRemoval(portalId, links) {
	var linksToSave = new Array();
	for(var id in links) {
		linksToSave[linksToSave.length] = links[id].edgeGuid;
	}
	if(linksToSave.length > 0)
		queueQuery('/*flagSomeLinks*/UPDATE links SET flagged = 1 WHERE (originid = ? or otherid = ?) and id not in (?)',
		[portalId,portalId,linksToSave]);
	else 
		queueQuery('/*flagAllLinks*/UPDATE links SET flagged = 1 WHERE (originid = ? or otherid = ?)',
		[portalId,portalId]);
		
}

function deleteFlaggedLinks() {
	queueQuery('/*dlink*/DELETE FROM links WHERE flagged=1',
	[]);
}

/******************** RESONATOR UPDATES *****************/

function deleteResonator(portalid,slot) {
	//console.log("delete slot " + slot);
	queueQuery('/*drez*/DELETE FROM resonators WHERE portalid = ? AND slot = ?',
		[portalid, slot]);
}

var urcache = new Cache(16000);

function updateResonator(id, portalid, slot, playerid, distance, level, energy) {
	if(urcache.getItem(id + portalid + slot + playerid + distance + level + energy) != null)
		return;
	urcache.setItem(id + portalid + slot + playerid + distance + level + energy,"done");
	
	//console.log("update slot " + slot);

	queueQuery('/*urez*/INSERT INTO resonators (id, portalid, slot, playerid, distance, level, energy) VALUES ' +
		'(?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE id=VALUES(id), playerid = VALUES(playerid), distance = VALUES(distance), level = VALUES(level), energy = VALUES(energy) ',
		[id, portalid, slot, playerid, distance, level, energy]);
}

/******************** MOD UPDATES *****************/

var moducache = new Cache(2000);

function updateMod(portalid, slot, type, rarity, playerid) {
	if(moducache.getItem(portalid + slot + type + rarity + playerid) != null)
		return;
	moducache.setItem(portalid + slot + type + rarity + playerid,"done");
	
	queueQuery('/*umod*/INSERT INTO mods (portalid, slot, type, rarity, playerid) VALUES (?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE type=VALUES(type), rarity=VALUES(rarity),playerid=VALUES(playerid) ',
	[portalid,slot,type,rarity,playerid]);
}

function deleteMod(portalid, slot) {
	queueQuery('/*dmod*/DELETE FROM mods WHERE portalid = ? AND slot = ?',
		[portalid, slot]);
}

/**************** PLAYER UPDATES ******************/

var playerlcache = new Cache(800);

function updatePlayerLevel(id, level) {
	if(playerlcache.getItem(id + level) != null)
		return;
	playerlcache.setItem(id + level,"done");
	
	queueQuery('/*pl*/INSERT INTO players (id, level) VALUES (?, ?) ON DUPLICATE KEY UPDATE level = IF(level > VALUES(level), level, VALUES(level)) ',
	[id,level]);
}

var playertcache = new Cache(800);

function updatePlayerTeam(id, team) {
	if(playertcache.getItem(id + team) != null)
		return;
	playertcache.setItem(id + team,"done");
	
	queueQuery('/*pt*/INSERT INTO players (id, team) VALUES (?, ?) ON DUPLICATE KEY UPDATE team = VALUES(team) ',
	[id,team]);
}

var playerncache = new Cache(800);
	
function updatePlayerName(id, name) {
	if(playerncache.getItem(id + name) != null)
		return;
	playerncache.setItem(id + name,"done");
	
	queueQuery('/*pn*/INSERT INTO players (id, name) VALUES (?, ?) ON DUPLICATE KEY UPDATE name = VALUES(name) ',
	[id,name]);

	queueQuery('/*pname*/INSERT IGNORE INTO names (id, name) VALUES (?, ?)',
	[id,name]);
}

/**************** PORTAL UPDATES ******************/

var portaltocache = new Cache(3000);
//Do I really need this here?
function finalizePortal(portalId) {
	/*if(portaltocache.getItem(id) != null)
		return;
		
	var exp = new Date();
	exp.setMinutes(new Date().getMinutes() + 30);
	portaltocache.setItem(id,"done", {expirationAbsolute:exp});*/
	var d = new Date();
	
	
	queueQuery('/*updatePortalLinks*/UPDATE portals SET links = (select count(1) from links where originid = ? or otherid = ?)  where id = ?',
		[portalId,portalId,portalId]);
		
	queueQuery('/*updatePortalFields*/UPDATE portals SET fields = ' + 
	'(select count(1) from fields where portalid1 = ? or portalid2 = ? or portalid3 = ?) where id = ?',
		[portalId,portalId,portalId,portalId]);
	
	queueQuery('/*portalUpdated*/UPDATE portals SET updated = ?, attempts = 0 WHERE id = ? ',
	[d.getTime(),portalId]);
}

var portalncache = new Cache(3000);

function updatePortalName(id, name) {
	if(portalncache.getItem(id + name) != null)
		return;
	portalncache.setItem(id + name,"done");
	
	queueQuery('/*Pn*/INSERT INTO portals (id, name) VALUES (?, ?) ON DUPLICATE KEY UPDATE name = VALUES(name) ',
	[id,name]);
}

var portaltcache = new Cache(2000);

function updatePortalTeam(id, team) {
	if(portaltcache.getItem(id + team) != null)
		return;
	portaltcache.setItem(id + team,"done");
	
	queueQuery('/*Pt*/INSERT INTO portals (id, team) VALUES (?, ?) ON DUPLICATE KEY UPDATE team = VALUES(team) ',
	[id,team]);
}

var portalllcache = new Cache(5000);

function updatePortalLatLong(id, lat, lng) {
	if(portalllcache.getItem(id + lat + lng) != null)
		return;
	portalllcache.setItem(id + lat + lng,"done");
	
	queueQuery('/*Pll*/INSERT INTO portals (id, latitude, longitude) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE latitude = VALUES(latitude),longitude = VALUES(longitude) ',
	[id,lat,lng]);
}

var portalccache = new Cache(2000);

function updatePortalCaptured(id, captured) {
	if(portalccache.getItem(id + captured) != null)
		return;
	portalccache.setItem(id + captured,"done");
	
	queueQuery('/*Pc*/INSERT INTO portals (id, captured) VALUES (?, ?) ON DUPLICATE KEY UPDATE captured = VALUES(captured)',
	[id,captured]);
}

/**************** PLEXT INSERTS ******************/

var plextcache = new Cache(10000);

function updatePlext(id, playerid, type, action, portalid1, portalid2, timestamp) {
	if(plextcache.getItem(id) != null)
		return;
	plextcache.setItem(id,"done");
	
	queueQuery('INSERT IGNORE INTO plexts(id,playerid,type,action,portal1,portal2,epoch) VALUES (?,?,?,?,?,?,?) ',
	[id,playerid,type,action,portalid1,portalid2,timestamp]);
}