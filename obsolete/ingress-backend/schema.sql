CREATE TABLE `fields` (
  `id` char(40) NOT NULL,
  `portalid1` char(40) DEFAULT NULL,
  `portalid2` char(40) DEFAULT NULL,
  `portalid3` char(40) DEFAULT NULL,
  `team` char(15) DEFAULT NULL,
  `mu` int(11) DEFAULT NULL,
  `playerid` char(40) DEFAULT NULL,
  `flagged` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `links` (
  `id` char(40) NOT NULL,
  `originid` char(40) DEFAULT NULL,
  `otherid` char(40) DEFAULT NULL,
  `flagged` tinyint(1) DEFAULT NULL,
  `playerid` char(40) DEFAULT NULL,
  `team` char(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `mods` (
  `portalid` char(40) NOT NULL DEFAULT '',
  `slot` int(11) NOT NULL DEFAULT '0',
  `type` char(20) DEFAULT NULL,
  `rarity` char(20) DEFAULT NULL,
  `playerid` char(40) DEFAULT NULL,
  PRIMARY KEY (`portalid`,`slot`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `names` (
  `id` char(40) NOT NULL,
  `name` char(40) NOT NULL,
  PRIMARY KEY (`id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `nodes` (
  `id` char(20) NOT NULL DEFAULT '',
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `players` (
  `id` char(40) NOT NULL DEFAULT '',
  `name` char(20) NOT NULL DEFAULT '',
  `team` char(15) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `plexts` (
  `id` char(40) NOT NULL DEFAULT '',
  `playerid` char(40) DEFAULT NULL,
  `type` char(20) DEFAULT NULL,
  `action` char(20) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `portal1` char(40) DEFAULT NULL,
  `epoch` bigint(20) DEFAULT NULL,
  `portal2` char(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `portals` (
  `id` char(40) NOT NULL DEFAULT '',
  `team` char(15) DEFAULT NULL,
  `name` char(40) DEFAULT NULL,
  `latitude` int(11) DEFAULT NULL,
  `longitude` int(11) DEFAULT NULL,
  `captured` bigint(20) DEFAULT NULL,
  `assigned` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated` varchar(20) DEFAULT NULL,
  `fields` int(3) DEFAULT NULL,
  `links` int(3) DEFAULT NULL,
  `attempts` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `resonators` (
  `portalid` char(40) NOT NULL DEFAULT '',
  `slot` int(11) NOT NULL DEFAULT '0',
  `id` char(40) DEFAULT NULL,
  `playerid` char(40) DEFAULT NULL,
  `distance` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `energy` int(11) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`portalid`,`slot`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;











