var mysql      = require('mysql');
var pool = mysql.createPool({
		host     : 'localhost',
		user     : 'ingress',
		password : 'ingress12345',
		database : 'ingress_data'
	});

/*
// /api/playerdata/:date
exports.playerdata = function(req, res){
	pool.getConnection(function(err, connection) {
		connection.query('select count(1) as playerCount from players where ' +
						'players.id in (select playerid from plexts, portals where portal1=portals.id and ' +
						'(latitude between 40075000 and 40837000) and (longitude between -112099000 ' +
						'and -111529000))', "" , function(err, result) {
			console.log("SQLERR[count]:" + err);
			
			var pcout = result[0].playerCount;
			
			connection.query("select count(1) as count from plexts,portals " +
				"where FROM_UNIXTIME(epoch/1000, '%Y%m%d') = ? and portal1=portals.id " +
				"and (latitude between 40075000 and 40837000) and (longitude between -112099000 and -111529000)",
				req.params.date , function(err, result) {
				console.log("SQLERR[actions]:" + err);
				res.json({playerCount : pcout, playerActionCount : result[0].count});
			});
		});
	});
};*/

/** /api/map/players?latU=&latL=&longU=&longL= **/
//API to get a list of players for the team (RESISTANCE/ALIENS/NEUTRAL)
exports.players = function(req, res){
	var lat1 = req.query.latL;
    var lat2 = req.query.latU;
    var long1 = req.query.longL;
    var long2 = req.query.longU;
    
	pool.getConnection(function(err, connection) {
		//console.log(lat1+long1+"to"+lat2+long2);
		var query = connection.query("select distinct players.id as id, players.name as name, players.team as team, players.level as level from plexts,players,portals " +
						"where players.id=playerid " +
						"and portal1=portals.id " +
						"and (latitude between ? and ?) and (longitude between ? and ?) " +
						"" +
						"", [lat1,lat2,long1,long2] , function(err, result) {
			connection.end();
			res.json({ players : result });
		});
	});
};

/** /api/map/portals?latU=&latL=&longU=&longL= **/
//API to get a list of portals for the latlng
exports.portals = function(req, res){
	var team = req.params.team;
	var lat1 = req.query.latL;
    var lat2 = req.query.latU;
    var long1 = req.query.longL;
    var long2 = req.query.longU;
    
	pool.getConnection(function(err, connection) {
		connection.query("select " +
		"portals.id, portals.team, portals.name, portals.links, portals.fields, " +
		"portals.captured, portals.updated,portals.latitude,portals.longitude, " +
		//Reso fields
		"rzero.level  as rl0, " +
		"rone.level   as rl1, " +
		"rtwo.level   as rl2, " +
		"rthree.level as rl3, " +
		"rfour.level  as rl4, " +
		"rfive.level  as rl5, " +
		"rsix.level   as rl6, " +
		"rseven.level as rl7, " +
		//Mod fields
		"mzero.type  as mt0, " +
		"mone.type   as mt1, " +
		"mtwo.type   as mt2, " +
		"mthree.type as mt3, " +
		"mzero.rarity  as mr0, " +
		"mone.rarity   as mr1, " +
		"mtwo.rarity   as mr2,  " +
		"mthree.rarity as mr3 " +
		"from portals " +
		//Join Resos
		"left outer join  resonators rzero  on rzero.portalid=portals.id  and rzero.slot=0 " +
		"left outer join  resonators rone   on rone.portalid=portals.id   and rone.slot=1 " +
		"left outer join  resonators rtwo   on rtwo.portalid=portals.id   and rtwo.slot=2 " +
		"left outer join  resonators rthree on rthree.portalid=portals.id and rthree.slot=3 " +
		"left outer join  resonators rfour  on rfour.portalid=portals.id  and rfour.slot=4 " +
		"left outer join  resonators rfive  on rfive.portalid=portals.id  and rfive.slot=5 " +
		"left outer join  resonators rsix   on rsix.portalid=portals.id   and rsix.slot=6 " +
		"left outer join  resonators rseven on rseven.portalid=portals.id and rseven.slot=7 " +
		//Join Mods
		"left outer join  mods mzero  on mzero.portalid=portals.id  and mzero.slot=0 " +
		"left outer join  mods mone   on mone.portalid=portals.id   and mone.slot=1 " +
		"left outer join  mods mtwo   on mtwo.portalid=portals.id   and mtwo.slot=2 " +
		"left outer join  mods mthree on mthree.portalid=portals.id and mthree.slot=3 " +
		"where (latitude between ? and ?) " +
		"and (longitude between ? and ?) and attempts < 3",
		[lat1,lat2,long1,long2] , function(err, result) {
			console.log(JSON.stringify(err));
			connection.end();
			res.json({ portals : result });
		});
	});
};
/*
// /api/map/player/:id
exports.player = function(req, res){
console.log("Get player:"+req.params.id);

	pool.getConnection(function(err, connection) {
		var query = connection.query("select level, name, id, team from players " +
						"where id=?", [req.params.id] , function(err, result) {
			console.log("SQLERR[stat]:" + err);
			console.log("result:" + JSON.stringify(result));
			res.json({ player : result[0] });
		});
		console.log(query.sql);
	});
};

// /api/map/portal/:id
exports.portal = function(req, res){
console.log("Get portal:"+req.params.id);

	pool.getConnection(function(err, connection) {
		var query = connection.query("select floor(sum(level)/8) as level, portals.name, portals.id, portals.team from portals, resonators " +
						"where portals.id = ? and resonators.portalid = portals.id group by resonators.portalid", [req.params.id] , function(err, result) {
			console.log("SQLERR[stat]:" + err);
			console.log("result:" + JSON.stringify(result));
			res.json({ portal : result[0] });
		});
		console.log(query.sql);
	});
};

// /api/plext/player/:id
exports.playerPlexts = function(req, res){

	pool.getConnection(function(err, connection) {
		var query = connection.query("select FROM_UNIXTIME(epoch/1000) as time, action, type, portals.id as portalid, portals.name as portalname from plexts,players,portals " +
						"where players.id=playerid " +
						"and portal1=portals.id " +
						"and players.id=? " +
						"order by epoch desc", [req.params.id] , function(err, result) {
			console.log("SQLERR[stat]:" + err);
			res.json({ plexts : result });
		});
	});
};
// /api/plext/portal/:id
exports.portalPlexts = function(req, res){

	pool.getConnection(function(err, connection) {
		var query = connection.query("select FROM_UNIXTIME(epoch/1000) as time, action, type, players.id as playerid, players.name as name from plexts,players,portals " +
						"where players.id=playerid " +
						"and portal1=portals.id " +
						"and portals.id=? " +
						"order by epoch desc", [req.params.id] , function(err, result) {
			console.log("SQLERR[stat]:" + err);
			res.json({ plexts : result });
		});
	});
};*/

/** /api/nextportal/:node?latU=&latL=&longU=&longL= **/
//Call for client to get next portal to scan
//If one is available, mark it as assigned for 5 min
exports.nextportal = function(req, res){
	var node = req.params.node;
	var latL = req.query.latL;
    var latU = req.query.latU;
    var longL = req.query.longL;
    var longU = req.query.longU;
	
	// Get next portal
	pool.getConnection(function(err, connection) {
		connection.query(
			"select * from portals where " +
			"(latitude between ? and ?) and " +
			"(longitude between ? and ?) and " +
			"(updated is null or updated < NOW() - INTERVAL 6 HOUR) and " +
			"assigned < NOW() - INTERVAL 20 MINUTE and attempts < 5 " +
			"order by updated asc limit 1", [latL,latU,longL,longU] , function(err, result) {
			
			//All portals are up to date in the scan range
			if(result.length <= 0) {
				connection.end();
				res.json({ data : "done" });
			} else { //Assign portal and return for scanning
				connection.query("update portals set assigned = now(), attempts = attempts + 1 where id = ?", [result[0].id] , function(err, result) {
						connection.end();
					});
				res.json({ data : result[0] });
			}
		});
	});
	
	// Set that this node has updated now to keep track of nodes connected
	pool.getConnection(function(err, connection) {
		connection.query("insert into nodes (id, updated) values (?, now()) on duplicate key update updated = now()",[node], function(err, result) {
				connection.end();
			});
	});
};

//Initialize gearman
var Gearman = require("node-gearman");
var gearman = new Gearman();

//Gearman logging
gearman.on("connect", function(){
    console.log("Gearman connected!");
});

//Gearman logging
gearman.on("error", function(){
    console.log("Gearman error");
});

/** /api/postdata/:node **/
//For extension to post data to server from intel map
exports.postdata = function(req, res) {
	console.log("GearnodeRequest");
	//Submit job to gearman
	gearman.submitJob("parse", JSON.stringify(req.body));
	//Send response to client
	res.end('<html><head><title>200 - OK</title></head><body><h1>Not found.</h1></body></html>');
};

/*
exports.heatmap = function(req, res) {

	pool.getConnection(function(err, connection) {
		var query = connection.query("select latitude as lat, longitude as lon, count(1) as value from plexts,portals where portal1=portals.id group by latitude,longitude",
		[] , function(err, result) {
			console.log("SQLERR[heatmap]:" + err);
			res.json({ heatmap : result });
		});
	});
}

exports.heatmapPlayer = function(req, res) {
	pool.getConnection(function(err, connection) {
		connection.query("select latitude as lat, longitude as lon, count(1) as value from plexts,portals where portal1=portals.id and playerid = ? group by latitude,longitude",
		[req.params.id] , function(err, result) {
			console.log("SQLERR[heatmap]:" + err);
			res.json({ heatmap : result });
		});
	});
}
*/