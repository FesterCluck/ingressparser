'use strict';

function defaultCookies($cookies) {
	if(typeof $cookies.latitudeLower == 'undefined') {
		$cookies.latitudeLower = "36999076";
		$cookies.longitudeLower = "-114041721";
		$cookies.latitudeUpper = "41993717";
		$cookies.longitudeUpper = "-109045216";
        console.log("ResetCookies Index");
	}
}

/* Controllers */

function SummaryCtrl($scope, $http, $cookies) {
	console.log("SummaryCtrl");
	
	//Default cookies
	defaultCookies($cookies);
    
    //Reload when lat long chages
    $scope.$on('latlong', function() {
        console.log("latlonggot");
		reloadSummaryCtrl($scope, $http, $cookies);
    });

    //reload when changing current date
	$scope.change = function() {
		reloadSummaryCtrl($scope, $http, $cookies);
	};
    
    $http.get('/api/plext/dates').
    success(function(data, status, headers, config) {
      $scope.availableDates = data.dates;
	  $scope.currentDate = $scope.availableDates[$scope.availableDates.length-1];
	  reloadSummaryCtrl($scope, $http, $cookies)
    });
    
    $http.get('/api/map/teamBalance').
    success(function(data, status, headers, config) {
      $scope.teams = data.balance;
      $scope.portals = data.portals;
    });
}

function reloadSummaryCtrl($scope, $http, $cookies) {
    $http.get('/api/plext/dateblock/' + $scope.currentDate.date).
    success(function(data, status, headers, config) {
        var blocks = new Array();
        for(var i = 0; i < 289; i++) {
            blocks[i] = new Object();
            blocks[i].present = "notpresent";
            blocks[i].timeblock = i;
        }
        for(var id in data.dates) {
            var block = data.dates[id];
            blocks[block.timeblock].present = "present";
            blocks[block.timeblock].timeblock = block.timeblock;
        }
        //console.log(blocks);
        $scope.timeBlocks = blocks;
    });

    $http.get('/api/playerdata/' + $scope.currentDate.date).
    success(function(data, status, headers, config) {
      $scope.playerCount = data.playerCount;
      $scope.playerActionCount = data.playerActionCount;
    });

   $http.get('/api/plext/top/' + $scope.currentDate.date + '/A/A'
    +'?latU='+$cookies.latitudeUpper
    +'&longU='+$cookies.longitudeUpper
    +'&latL='+$cookies.latitudeLower
    +'&longL='+$cookies.longitudeLower).
    success(function(data, status, headers, config) {
      $scope.topActive = data.stat;
    });
	
  $http.get('/api/plext/top/' + $scope.currentDate.date + '/CAPTURED/PORTAL'
    +'?latU='+$cookies.latitudeUpper
    +'&longU='+$cookies.longitudeUpper
    +'&latL='+$cookies.latitudeLower
    +'&longL='+$cookies.longitudeLower).
    success(function(data, status, headers, config) {
      $scope.topCappers = data.stat;
    });
	
  $http.get('/api/plext/top/' + $scope.currentDate.date + '/CREATED/LINK'
    +'?latU='+$cookies.latitudeUpper
    +'&longU='+$cookies.longitudeUpper
    +'&latL='+$cookies.latitudeLower
    +'&longL='+$cookies.longitudeLower).
    success(function(data, status, headers, config) {
      $scope.topLinkers = data.stat;
    });
	
  $http.get('/api/plext/top/' + $scope.currentDate.date + '/DESTROYED/LINK'
    +'?latU='+$cookies.latitudeUpper
    +'&longU='+$cookies.longitudeUpper
    +'&latL='+$cookies.latitudeLower
    +'&longL='+$cookies.longitudeLower).
    success(function(data, status, headers, config) {
      $scope.topUnlinkers = data.stat;
    });
	
  $http.get('/api/plext/top/' + $scope.currentDate.date + '/CREATED/CONTROL_FIELD'
    +'?latU='+$cookies.latitudeUpper
    +'&longU='+$cookies.longitudeUpper
    +'&latL='+$cookies.latitudeLower
    +'&longL='+$cookies.longitudeLower).
    success(function(data, status, headers, config) {
      $scope.topFielders = data.stat;
    });
	
  $http.get('/api/plext/top/' + $scope.currentDate.date + '/DESTROYED/CONTROL_FIELD'
    +'?latU='+$cookies.latitudeUpper
    +'&longU='+$cookies.longitudeUpper
    +'&latL='+$cookies.latitudeLower
    +'&longL='+$cookies.longitudeLower).
    success(function(data, status, headers, config) {
      $scope.topUnfielders = data.stat;
    });
}

function PlayerListCtrl($scope, $http, $cookies) {
	console.log("PlayerListCtrl");
	defaultCookies($cookies);
    
    $scope.$on('latlong', function() {
        console.log("latlonggot");
		reloadPlayerListCtrl($scope, $http, $cookies);
    });

    reloadPlayerListCtrl($scope, $http, $cookies);
}

function reloadPlayerListCtrl($scope, $http, $cookies) {
    $http.get('/api/map/players'
    +'?latU='+$cookies.latitudeUpper
    +'&longU='+$cookies.longitudeUpper
    +'&latL='+$cookies.latitudeLower
    +'&longL='+$cookies.longitudeLower).
    success(function(data, status, headers, config) {
      $scope.players = data.players;

		$scope.currentPage = 0;
		$scope.pageSize = 20;
		$scope.numberOfPages=function(){
			return Math.ceil($scope.players.length/$scope.pageSize);                
		}
		$scope.players.sort(sortByPlayerLevel);
    });
}

/************ PORTAL LIST CTRL ****************/
function PortalListCtrl($scope, $http, $cookies) {
	console.log("PortalListCtrl");
	defaultCookies($cookies);

    $scope.$on('latlong', function() {
        console.log("latlonggot");
		reloadPortalListCtrl($scope, $http, $cookies);
    });

	reloadPortalListCtrl($scope, $http, $cookies);

	$scope.sortByLastSeen = function() {
		//Sort arrays
		$scope.portals.sort(sortByLastSeen);
	}

	$scope.sortByResonators = function() {
		//Sort arrays
		$scope.portals.sort(sortByResonators);
	}

	$scope.sortByLinks = function() {
		//Sort arrays
		$scope.portals.sort(sortByLinks);
	}

	$scope.sortByFields = function() {
		//Sort arrays
		$scope.portals.sort(sortByFields);
	}

	$scope.sortByLevel = function() {
		//Sort arrays
		$scope.portals.sort(sortByLevel);
	}

	$scope.sortByAP = function() {
		//Sort arrays
		$scope.portals.sort(sortByAP);
	}

	$scope.sortByPS = function() {
		//Sort arrays
		$scope.portals.sort(sortByPS);
	}
}

var sortByLastSeen = function(b,a) {
	if(b.suffix == a.suffix)
		return b.lastSeen - a.lastSeen;
	if(b.suffix == "m" && (a.suffix == "d" || a.suffix == "h"))
		return -1;
	if(b.suffix == "h" && a.suffix == "d")
		return -1;
	return 1;
}


function sortByPlayerLevel(a,b) {
	return b.level - a.level;
}

var sortByLevel = function(a,b) {
	if(b.level - a.level < 1 && b.level - a.level > -1)
		return sortByResonators(a,b);
	return b.level - a.level;
}

var sortByLinks = function(a,b) {
	return b.links - a.links;
}

var sortByFields = function(a,b) {
	return b.fields - a.fields;
}

var sortByAP = function(a,b) {
	return b.ap - a.ap;
}

var sortByPS = function(a,b) {
	return b.ps - a.ps;
}

var sortByResonators = function(b,a) {
	var ares = a.resonators;
	var bres = b.resonators;

	if(ares.length == 0 && bres.length == 0)
		return 0;

	for(var i = 0; i < 8; i++) {
		if(i >= bres.length && i >= ares.length)
			break;
		if(i >= bres.length)
			return 1;
		if(i >= ares.length)
			return -1;

		if(ares[i] < bres[i])
			return -1;
		if(ares[i] > bres[i])
			return 1;
	}

	return 0;
}

function reloadPortalListCtrl($scope, $http, $cookies) {
	var nowms = Date.now();
	var beforehttp = Date.now();
	
    $http.get('/api/map/portals'
    +'?latU='+$cookies.latitudeUpper
    +'&longU='+$cookies.longitudeUpper
    +'&latL='+$cookies.latitudeLower
    +'&longL='+$cookies.longitudeLower).
    success(function(data, status, headers, config) {
		var afterhttp = Date.now();
		$scope.portals = data.portals;

		$scope.currentPage = 0;
		$scope.pageSize = 20;
		$scope.numberOfPages=function(){
			return Math.ceil($scope.portals.length/$scope.pageSize);                
		}

		$scope.resistancePortals = new Array();
		$scope.alienPortals = new Array();
		$scope.neutralPortals = new Array();
	  
		$scope.averageTime = 0;

		$scope.resistance = new Object();
		$scope.resistance.links = 0;
		$scope.resistance.fields = 0;
		$scope.resistance.portals = 0;

		$scope.enlightened = new Object();
		$scope.enlightened.links = 0;
		$scope.enlightened.fields = 0;
		$scope.enlightened.portals = 0;

		$scope.neutral = new Object();
		$scope.neutral.links = 0;
		$scope.neutral.fields = 0;
		$scope.neutral.portals = 0;
		var maxHours = 0;
	  
		for(var i = 0; i < $scope.portals.length; i++) {
			var portal = $scope.portals[i];
			portal.lat = portal.latitude / 1000000;
			portal.lng = portal.longitude / 1000000;
			//console.log(JSON.stringify(portal));
			//Populate lastSeen
			portal.days = (nowms - portal.captured) / (1000 * 60 * 60 * 24);
			portal.days = portal.days.toFixed(2);
				
			portal.lastSeen = (nowms - portal.updated) / (1000 * 60);
			$scope.averageTime = portal.lastSeen + $scope.averageTime;
			
			portal.suffix = "m";
			
			if(portal.lastSeen > 60) {
				portal.lastSeen = portal.lastSeen / 60;
				portal.suffix = "h";
			}
			
			if(portal.lastSeen > 24 && portal.suffix == "h") {
				portal.lastSeen = portal.lastSeen / 24;
				portal.suffix = "d";
			}
			
			portal.lastSeen = portal.lastSeen.toFixed(2);
			
			populateMods(portal);
			populateResos(portal);
			calcAP(portal);
			
			if(portal.team == 'ENLIGHTENED')
				portal.team = 'ALIENS';

			//Assign to Team Array
			if(portal.team == 'ALIENS') {
				$scope.enlightened.portals = $scope.enlightened.portals + 1;
			} else if(portal.team == 'RESISTANCE') {
				$scope.resistance.portals = $scope.resistance.portals + 1;
			} else {
				portal.days = 0;
				$scope.neutral.portals = $scope.neutral.portals + 1;
			}
			calcPS(portal);
			
			var updated = (nowms - portal.updated) / (1000 * 60 * 60);
			if(updated > maxHours) {
				maxHours = updated;
			}
		}
		console.log("pph calc:"+$scope.portals.length+"p"+maxHours+"h");
		$scope.portalsPerHour = $scope.portals.length / maxHours;
		$scope.portalsPerHour = $scope.portalsPerHour.toFixed(2);
		
		var afterload = Date.now();

		//Sort array
		$scope.portals.sort(sortByLastSeen);
		
		var aftersort = Date.now();

		//Update average staleness
		$scope.averageTime = $scope.averageTime / $scope.portals.length;
		$scope.averageTime = $scope.averageTime / 60;
		$scope.averageTime = $scope.averageTime.toFixed(2);

		console.log("http:" + ((afterhttp-beforehttp) / 1000) + "s");
		console.log("load:" + ((afterload-afterhttp) / 1000) + "s");
		console.log("sort:" + ((aftersort-afterload) / 1000) + "s");
    });

}

var sortResos = function(a,b) {
	return b - a;
}

//Constants for AP value
var PLACING_RESONATORS = 1750;
var DESTROY_RESONATOR = 75;
var DESTROY_LINK = 187;
var DESTROY_FIELD = 750;

function calcAP(portal) {
	portal.ap = PLACING_RESONATORS;
    portal.ap = portal.ap + portal.resonators.length * DESTROY_RESONATOR;
    portal.ap = portal.ap + portal.links * DESTROY_LINK;
    portal.ap = portal.ap + portal.fields * DESTROY_FIELD;
}

function calcPS(portal) {
	portal.ps = portal.level / 2;
	portal.ps = portal.ps + portal.links;
	portal.ps = portal.ps + (portal.fields * 2);
	for(var id in portal.mods) {
		var mod = portal.mods[id];
		switch(mod.type + mod.rarity) {
			case "MC":
			case "HC":
				portal.ps = portal.ps + 1;break;
			case "MR":
			case "HR":
				portal.ps = portal.ps + 2;break;
			case "MV":
			case "HV":
				portal.ps = portal.ps + 3;break;
			case "SC":	
				portal.ps = portal.ps - 0.25;break;
			case "SR":
				portal.ps = portal.ps - 0.5;break;
			case "SV":
			case "AR":
			case "TR":
				portal.ps = portal.ps - 1;break;
			case "LR":
				portal.ps = portal.ps + 0.25;break;
			default:
				console.log("Unknown type+rarity: " + mod.type + mod.rarity);
		}
	}

	portal.ps = portal.ps + (portal.days / 5);
	portal.ps = portal.ps.toFixed(2);
}

function populateResos(portal) {
	var mods = new Array();
	
	if(portal.rl0 != null) {
		mods[mods.length] = portal.rl0;
	}
	if(portal.rl1 != null) {
		mods[mods.length] = portal.rl1;
	}
	if(portal.rl2 != null) {
		mods[mods.length] = portal.rl2;
	}
	if(portal.rl3 != null) {
		mods[mods.length] = portal.rl3;
	}
	if(portal.rl4 != null) {
		mods[mods.length] = portal.rl4;
	}
	if(portal.rl5 != null) {
		mods[mods.length] = portal.rl5;
	}
	if(portal.rl6 != null) {
		mods[mods.length] = portal.rl6;
	}
	if(portal.rl7 != null) {
		mods[mods.length] = portal.rl7;
	}

	portal.level = 0;

	for(var id in mods) {
		portal.level = portal.level + mods[id];
	}

	portal.level = Math.floor(portal.level / 8);

	mods.sort(sortResos);

	portal.resonators = mods;
}

var sortMods = function(b,a) {
	if(b.type > a.type)
		return -1;
	if(b.type < a.type)
		return 1;
	if(b.rarity > a.rarity)
		return -1;
	if(b.rarity < a.rarity)
		return 1;
	return 0;
}

function populateMods(portal) {
	var mods = new Array();
	
	if(portal.mt0 != null) {
		mods[mods.length] = getMod(portal.mt0, portal.mr0);
	}
	if(portal.mt1 != null) {
		mods[mods.length] = getMod(portal.mt1, portal.mr1);
	}
	if(portal.mt2 != null) {
		mods[mods.length] = getMod(portal.mt2, portal.mr2);
	}
	if(portal.mt3 != null) {
		mods[mods.length] = getMod(portal.mt3, portal.mr3);
	}

	mods.sort(sortMods);
	
	portal.mods = mods;
}

function getMod(type, rarity) {
	var mod = new Object();
	switch(type) {
		
		case "RES_SHIELD": 		mod.type = "S";break;
		case "FORCE_AMP": 		mod.type = "A";break;
		case "TURRET": 			mod.type = "T";break;
		case "HEATSINK": 		mod.type = "H";break;
		case "MULTIHACK": 		mod.type = "M";break;
		case "LINK_AMPLIFIER": 	mod.type = "L";break;
		default: console.log("Unknown Mod: [" + type + "]");
	}
	switch(rarity) {
		case "COMMON": 		mod.rarity = "C";break;
		case "RARE": 		mod.rarity = "R";break;
		case "VERY_RARE": 	mod.rarity = "V";break;
		default: console.log("Unknown Rarity: [" + rarity + "]");
	}
	return mod;
}

function HeaderCtrl($scope, $http, $cookies,$cookieStore, $rootScope) {
console.log("HeaderCtrl");
	defaultCookies($cookies);
    
    var ary = $cookieStore.get('areas');
    
    if(typeof ary == 'undefined') {
        ary = [];
        console.log("Init Cookies Areas");
        
		var area = new Object();
        area.name='Utah';
		area.latitudeLower = "36999076";
		area.longitudeLower = "-114041721";//40.3745, -111.816
		area.latitudeUpper = "41993717";
		area.longitudeUpper = "-109045216";//40.5289, -112038
        ary[ary.length] = area;
		
        var area = new Object();
        area.name='Wasatch Front';
		area.latitudeLower = "40075000";
		area.longitudeLower = "-112099000";//40.3745, -111.816
		area.latitudeUpper = "40837000";
		area.longitudeUpper = "-111529000";//40.5289, -112038
        ary[ary.length] = area;
        
        area = new Object();
        area.name='Peace Gardens';
		area.latitudeLower = "40744965";
		area.longitudeLower = "-111926411";
		area.latitudeUpper = "40750078";
		area.longitudeUpper = "-111913880";
        ary[ary.length] = area;
        
        area = new Object();
        area.name='RivertonLehi';
		area.latitudeLower = "40372706";
		area.longitudeLower = "-112213408";
		area.latitudeUpper = "40537025";
		area.longitudeUpper = "-111812407";
        ary[ary.length] = area;
        $cookieStore.put('areas',ary);
    }
    
    $scope.areas = ary;
    $scope.latlong1 = $cookies.latitudeLower + ',' + $cookies.longitudeLower;
    $scope.latlong2 = $cookies.latitudeUpper + ',' + $cookies.longitudeUpper;
	for(var i = 0; i < $scope.areas.length;i++) {
        var area = $scope.areas[i];
		if(area.latitudeLower==$cookies.latitudeLower&&
			area.longitudeLower==$cookies.longitudeLower&&
			area.latitudeUpper==$cookies.latitudeUpper&&
			area.longitudeUpper==$cookies.longitudeUpper)
			area.selected = true;
		else
			area.selected = false;
    }

    $scope.deleteArea = function(area) {
        //console.log("deleting " + area.name);
        var areas = $cookieStore.get('areas');
        for(var i = 0; i < areas.length;i++) {
            if(areas[i].name == area.name) {
                areas.splice(i,1);
                //console.log("spliced " + i);
                break;
            }
        }
        $cookieStore.put('areas',areas);
        
        for(var i = 0; i < $scope.areas.length;i++) {
            if($scope.areas[i].name == area.name) {
                $scope.areas.splice(i,1);
                //console.log("spliced " + i);
                break;
            }
        }
    }
    
    $scope.selectArea = function(area) {
		for(var i = 0; i < $scope.areas.length;i++) {
            $scope.areas[i].selected = false
        }
		area.selected = true;
        $cookies.latitudeLower = area.latitudeLower;
		$cookies.longitudeLower = area.longitudeLower;
		$cookies.latitudeUpper = area.latitudeUpper;
		$cookies.longitudeUpper = area.longitudeUpper;
        $rootScope.$broadcast('latlong');
    }
    
	$scope.update = function() {
        //console.log("update" + $scope.latlong1 +" " + $scope.latlong2);
        var comma = $scope.latlong1.indexOf(',');
        var lat1 = $scope.latlong1.substring(0,comma);
        var long1 = $scope.latlong1.substring(comma+1,$scope.latlong1.length);
        //console.log("lat:"+lat1+"log:"+long1);
        
        var comma = $scope.latlong2.indexOf(',');
        var lat2 = $scope.latlong2.substring(0,comma);
        var long2 = $scope.latlong2.substring(comma+1,$scope.latlong2.length);
        //console.log("lat:"+lat2+"log:"+long2);
        if(lat1 > lat2) {
            $cookies.latitudeLower = lat2;
            $cookies.latitudeUpper = lat1;
        } else {
            $cookies.latitudeLower = lat1;
            $cookies.latitudeUpper = lat2;
        }
        if(long1 < long2) {
            $cookies.longitudeLower = long2;
            $cookies.longitudeUpper = long1;
        } else {
            $cookies.longitudeLower = long1;
            $cookies.longitudeUpper = long2;
        }
        
        if($scope.areaname.length > 0) {
            console.log("saving");
            var area = new Object();
            area.name=$scope.areaname;
            area.latitudeLower = $cookies.latitudeLower;
            area.longitudeLower = $cookies.longitudeLower;
            area.latitudeUpper = $cookies.latitudeUpper;
            area.longitudeUpper = $cookies.longitudeUpper;
            
            var areas = $cookieStore.get('areas');
            areas[areas.length] = area;
            $cookieStore.put('areas',areas);
            
            $scope.areas[$scope.areas.length] = area;
        }
        
        $rootScope.$broadcast('latlong');
	};
}

function HeatmapCtrl($scope, $http, $cookies, $routeParams) {
    console.log("HeatmapCtrl");
    $http.get('/api/heatmap').
    success(function(data, status, headers, config) {
    
        var testData = new Object();
        testData.data = [];
        testData.max = 0;
        for(var i = 0; i < data.heatmap.length; i++) {
            if(data.heatmap[i].lat == null || data.heatmap[i].lon == null)
                continue;
            if(data.heatmap[i].value > testData.max)
                testData.max = data.heatmap[i].value;
            var pair = new Object();
            pair.lat = data.heatmap[i].lat/1000000;
            pair.lon = data.heatmap[i].lon/1000000;
            pair.value = data.heatmap[i].value;
            testData.data[testData.data.length] = pair;
        }
       
        console.log(JSON.stringify(testData.data));
        
        var baseLayer = L.tileLayer(
                    'http://{s}.tile.cloudmade.com/4410b1d37d0143b38df9302bddd0e5e0/997/256/{z}/{x}/{y}.png',{
                        attribution: 'Map data � <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery � <a href="http://cloudmade.com">CloudMade</a>',
                        maxZoom: 18
                    }
                );
        var heatmapLayer = L.TileLayer.heatMap({
                    radius: 20,
                    opacity: 0.8,
                    gradient: {
                        0.45: "rgb(0,0,255)",
                        0.55: "rgb(0,255,255)",
                        0.65: "rgb(0,255,0)",
                        0.95: "yellow",
                        1.0: "rgb(255,0,0)"
                    }
                });
        heatmapLayer.addData(testData.data);
        var overlayMaps = {
            'Heatmap': heatmapLayer
        };
 
        var controls = L.control.layers(null, overlayMaps, {collapsed: false});
 
        var map = new L.Map('heatmapArea', {
            center: new L.LatLng(testData.data[0].lat, testData.data[0].lon),
            zoom: 6,
            layers: [baseLayer, heatmapLayer]
        });
 
        controls.addTo(map);
 
        // make accessible for debugging
        //layer = heatmapLayer;
    });

}

function PlayerCtrl($scope, $http, $cookies, $routeParams) {
console.log("PlayerCtrl");
    $http.get('/api/map/player/' + $routeParams.id).
    success(function(data, status, headers, config) {
      $scope.player = data.player;
    });

    $http.get('/api/plext/player/' + $routeParams.id).
    success(function(data, status, headers, config) {
      $scope.plexts = data.plexts;
    });
    
    $http.get('/api/heatmap/player/' + $routeParams.id).
    success(function(data, status, headers, config) {
    
        var testData = new Object();
        testData.data = [];
        testData.max = 0;
        for(var i = 0; i < data.heatmap.length; i++) {
            if(data.heatmap[i].lat == null || data.heatmap[i].lon == null)
                continue;
            if(data.heatmap[i].value > testData.max)
                testData.max = data.heatmap[i].value;
            var pair = new Object();
            pair.lat = data.heatmap[i].lat/1000000;
            pair.lon = data.heatmap[i].lon/1000000;
            pair.value = data.heatmap[i].value;
            testData.data[testData.data.length] = pair;
        }
       
        console.log(JSON.stringify(testData.data));
        
        var baseLayer = L.tileLayer(
                    'http://{s}.tile.cloudmade.com/4410b1d37d0143b38df9302bddd0e5e0/997/256/{z}/{x}/{y}.png',{
                        attribution: 'Map data � <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery � <a href="http://cloudmade.com">CloudMade</a>',
                        maxZoom: 18
                    }
                );
        var heatmapLayer = L.TileLayer.heatMap({
                    radius: 20,
                    opacity: 0.8,
                    gradient: {
                        0.45: "rgb(0,0,255)",
                        0.55: "rgb(0,255,255)",
                        0.65: "rgb(0,255,0)",
                        0.95: "yellow",
                        1.0: "rgb(255,0,0)"
                    }
                });
        heatmapLayer.addData(testData.data);
        var overlayMaps = {
            'Heatmap': heatmapLayer
        };
 
        var controls = L.control.layers(null, overlayMaps, {collapsed: false});
 
        var map = new L.Map('heatmapArea', {
            center: new L.LatLng(testData.data[0].lat, testData.data[0].lon),
            zoom: 10,
            layers: [baseLayer, heatmapLayer]
        });
 
        controls.addTo(map);
 
        // make accessible for debugging
        //layer = heatmapLayer;
    });
}

function PortalCtrl($scope, $http, $cookies, $routeParams) {
console.log("PortalCtrl");
    $http.get('/api/map/portal/' + $routeParams.id).
    success(function(data, status, headers, config) {
      $scope.portal = data.portal;
    });

    $http.get('/api/plext/portal/' + $routeParams.id).
    success(function(data, status, headers, config) {
      $scope.plexts = data.plexts;
    });
}