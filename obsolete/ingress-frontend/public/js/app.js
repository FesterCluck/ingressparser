var myModule = angular.module('myApp', ['ngCookies']).
  config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider.
      when('/', {
        templateUrl: 'portallist',
        controller: PortalListCtrl
      }).when('/playerlist', {
        templateUrl: 'playerlist',
        controller: PlayerListCtrl
      }).when('/portallist', {
        templateUrl: 'portallist',
        controller: PortalListCtrl
      }).when('/player/:id', {
        templateUrl: 'player',
        controller: PlayerCtrl
      }).when('/portal/:id', {
        templateUrl: 'portal',
        controller: PortalCtrl
      }).when('/heatmap', {
        templateUrl: 'heatmap',
        controller: HeatmapCtrl
      }).
      otherwise({
        redirectTo: '/'
      });
    $locationProvider.html5Mode(true);
  }]);

myModule.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});