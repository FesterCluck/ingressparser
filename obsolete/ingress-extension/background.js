console.log("background loaded");

var timeout = 0;

var ports = {};

chrome.extension.onConnect.addListener(function(port) {
    if (port.name !== "devtools")
		return;
		
	console.log("Port Connected");
    ports[port.portId_] = port;
	
    // Remove port when destroyed (eg when devtools instance is closed)
    port.onDisconnect.addListener(function(port) {
		console.log("Port Disconnected");
        delete ports[port.portId_];
    });
	
	//DT -> BG
    port.onMessage.addListener(function(msg) {
		console.log("DT -> BG " + JSON.stringify(msg));
        if(msg.msg == 'load') {
			chrome.tabs.get(msg.tabid, function (tab) {
				var tabUrl = encodeURIComponent(tab.url);
				var tabTitle = encodeURIComponent(tab.title);
				chrome.tabs.update(tab.id, {url: "http://www.ingress.com/intel?latE6=" + msg.lat + "&lngE6=" + msg.lng + "&z=17"});
			});
		} else if (msg.msg == 'scan') {
			chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
				//BG -> CS
				console.log("BG->CS: " + JSON.stringify(msg));
				chrome.tabs.sendMessage(tabs[0].id, msg, function(response) {});  
			});
			
			//Just in case page never loads, send a done back to devtools
			timeout = setTimeout(function() {console.log("Timed out. Notifying DevTools.");notifyDevtools({msg:"done"}); timeout = 0;},40000);
		} else if (msg.msg == 'clear') {
			chrome.tabs.get(msg.tabid, function (tab) {
				var tabUrl = encodeURIComponent(tab.url);
				var tabTitle = encodeURIComponent(tab.title);
				chrome.tabs.update(tab.id, {url: "about:blank"});
			});
		}
    });
});

//BG -> DT
function notifyDevtools(msg) {
	console.log("BG->DT: " + JSON.stringify(msg));
    Object.keys(ports).forEach(function(portId_) {
        ports[portId_].postMessage(msg);
    });
}

function log(text) {
	var msg = new Object();
	msg.msg = "log";
	msg.log = text;
	notifyDevtools(msg);
}

//CS -> BG
chrome.extension.onMessage.addListener(
  function(request, sender, sendResponse) {
	console.log("CS->BG: " + JSON.stringify(request));
	if(request.msg == 'done') {
		//Clear failsafe timeout if we finished ok
		if(timeout != 0) {
			clearTimeout(timeout);
		}
	}
	
	notifyDevtools(request);
    
    sendResponse();
});
 