
//Angular module setup and pathing
var myModule = angular.
  module('ingress-app', []).
  factory('MyService', function($rootScope,$http) {
        return {
            nextPortal: function() {
                log("Beginning next portal scan in (" + scantime + " * " + backoff + ") " + backoff*scantime + " seconds");
				clearTab();
				setTimeout(function() {control($http);}, backoff*scantime*1000);
            },
			log: function(text) {
                if(typeof $rootScope.log == 'undefined')
					$rootScope.log = [];
				$rootScope.log[$rootScope.log.length] = text;
				if($rootScope.log.length > 30) {
					$rootScope.log.splice(0,$rootScope.log.length - 30);
				}
				/*$rootScope.log.splice(0,0,text);
				if($rootScope.log.length > 30) {
					$rootScope.log.length = 20
				}*/
				$rootScope.$apply();
				var objDiv = document.getElementById("logbox");
				objDiv.scrollTop = objDiv.scrollHeight;
            } 			
        }
    });

//This injector will allow non-angular code to access the service and send broadcasts
var injector;
angular.element(document).ready(function() {
  injector = angular.bootstrap(document, ['ingress-app']);
});

var nextPortal = function() {
	injector.invoke(['MyService', function(MyService){
        MyService.nextPortal();
    }]);
}

var log = function(text) {
	injector.invoke(['MyService', function(MyService){
        MyService.log(text);
    }]);
}

var maxLatE6 = 0;
var maxLngE6 = 0;
var minLatE6 = 0;
var minLngE6 = 0;

var running = true;

//Send JSON to server
chrome.experimental.devtools.network.onRequestFinished.addListener(function (resource) 
{
	//Scan for lat long bounds
	for (var key in resource.request.headers) {
		var header = resource.request.headers[key];
		
		if(header.name.toLowerCase() == 'content-type' && header.value.indexOf("application/json") !== -1) {
            var obj = JSON.parse(resource.request.postData.text);
            if(typeof obj.maxLatE6 != 'undefined') {
				if(maxLatE6 != obj.maxLatE6 ||
				   maxLngE6 != obj.maxLngE6 ||
				   minLatE6 != obj.minLatE6 ||
				   minLngE6 != obj.minLngE6) {
				   
					maxLatE6 = obj.maxLatE6;
					maxLngE6 = obj.maxLngE6;
					minLatE6 = obj.minLatE6;
					minLngE6 = obj.minLngE6;
					log("DT: Window set to Lat " + minLatE6 + " " + maxLatE6 + " Lon " + minLngE6 + " " + maxLngE6);
				}
            }
        }
	}
	
	//If we're not scanning, don't send data up. It will overload the server
	//if(!running)
	//	return;
	
	for (var key in resource.request.headers) {
		var header = resource.request.headers[key];
		
		//If we're JSON response
		if(header.name == 'Content-Type' && header.value.indexOf("application/json") !== -1){
			//console.log("Getting cross site scripting permission");
			//Post for cross site scripting
			/*var xhr2 = new XMLHttpRequest();
			xhr2.open("POST", "http://209.141.53.197:8080/api/postdata/" + nodeid, false);
			xhr2.setRequestHeader('Content-Type', 'application/json');
			xhr2.onreadystatechange = function () {
				if (xhr2.readyState == 4 && xhr2.status == 200) {
					
				}
			}*/
			//get and post content
					resource.getContent(function(content, encoding) {
						log("DT: Posting data to server");
						var xhr = new XMLHttpRequest();
						xhr.open("POST", "http://lerasium.threesixtydev.com/api/postdata/" + nodeid , false);
						xhr.setRequestHeader('Content-Type', 'application/json');
						xhr.onreadystatechange = function () {
							if (xhr.readyState == 4 && xhr.status == 200) {
							}
						}
				
						xhr.send(content);
					});
		}
	}
});

//For communication with BG page
var port = chrome.extension.connect({name:"devtools"});

    port.onMessage.addListener(function(msg) {
		//console.log(msg);
		if(msg.msg == 'done') {
			log("DT: Got Done Message From Background");
			if(running)
				nextPortal();
			else
				log("DT: Stopping scan");
		} else if(msg.msg == 'log') {
			log(msg.log);
		}
    });

/**
Angular controllers for the devtools panel
**/

var scanLog = function() {
	log("DT: Sending Scan Message To Background Page");
	port.postMessage({msg:"scan"});
}

var backoff = 1;
var nodeid = "default";
var latU = 0;
var longU = 0;
var latL = 0;
var longL = 0;
var scantime = 30;

function clearTab() {
	log("DT: Sending Clear Message To Background Page");
	port.postMessage({msg:"clear","tabid":chrome.devtools.inspectedWindow.tabId, "lat":"","lng":""});
}

var control = function($http) {
    log("DT: Polling Server for Next Portal");
	
    $http.get('http://lerasium.threesixtydev.com/api/nextportal/' 
	+ nodeid 
	+'?latU='+latU
    +'&longU='+longU
    +'&latL='+latL
    +'&longL='+longL).
    success(function(data, status, headers, config) {
		log("DT: Server Response");
		
        if(data.data == 'done') {
			backoff = backoff + backoff;
			log("DT: No portals to scan. Backing off multiplier set to " + backoff);
        } else {
			log("DT: Scanning [" + data.data.name + "]");
			
			backoff = 1;
			
			log("DT: Sending Load Message To Background Page");
			port.postMessage({msg:"load","tabid":chrome.devtools.inspectedWindow.tabId, "lat":data.data.latitude,"lng":data.data.longitude});
			
			setTimeout(scanLog,15000);
			//setTimeout(nextPortal,15000);
        }
    }).
	error(function(data, status, headers, config) {
		log("DT: Failed to contact server");
		backoff = backoff + backoff;
        log("DT: Attempting next portal scan in (" + scantime + " * " + backoff + ") " + backoff*scantime + " seconds");
        setTimeout(function() {control($http);}, backoff * scantime * 1000);
	});
}

function HeaderCtrl($scope, $http) {

	$scope.nodeid = "default";
	$scope.scantime = 30;
	
    $scope.start = function() {
		/*latU = maxLatE6;
		latL = minLatE6;
		longU = maxLngE6;
		longL = minLngE6;*/
		latU = 41993717;
		latL = 36999076;
		longU = -109045216;
		longL = -114041721;
		scantime = $scope.scantime;
		nodeid = $scope.nodeid;
		log("DT: start");
        $scope.status = 'running';
		running = true;
        setTimeout(function() {control($http);}, 100);
    }
    
    $scope.stop = function() {
		log("DT: stop");
        $scope.status = 'stopping';
		running = false;
    }
}